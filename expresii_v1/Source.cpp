// expresii logice == != < > <= >=
// ! && ||

#include <iostream>
using namespace std;

int main(){
	int a=7;
	cout << (3<=a<=5) << endl; // gresit
	cout << ((3<=a) && (a<=5)) << endl; // din interval
	cout << ((a<3) || (a>5)) << endl;//din afara intervalului
}